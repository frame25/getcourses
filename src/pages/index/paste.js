var isFramed = window != window.top || document != top.document || self.location != top.location;

if (!isFramed) {
    document.addEventListener('DOMContentLoaded', function(){
        // styles
        var l = document.createElement('link');
        l.rel='stylesheet';
        l.href ='https://dev.some-code.ru/johnrid/css/index.css';
        document.head.appendChild(l);

        var s = document.createElement('script');
        s.src = 'https://dev.some-code.ru/johnrid/js/index.js';
        document.body.appendChild(s);
    })
}
