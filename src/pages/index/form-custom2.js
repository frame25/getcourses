var doc = this.document;
doc.addEventListener('DOMContentLoaded', function(){
    var standartPack = doc.querySelector('.form-position');
    var standartInput = standartPack.getElementsByTagName('input')[0];
    var priceBlock = doc.querySelector('.custom-show-price');
    var price = standartPack.querySelector('.form-position-price').innerHTML;
    var vacant = standartPack.querySelector('.stream-vacant').outerHTML;
    priceBlock.innerHTML = price;
    priceBlock.insertAdjacentHTML('afterbegin', vacant);
    standartInput.click();
    var sPrice = standartPack.querySelector('.final-price');
    var sTitle = standartPack.querySelector('.offer-title');

    sTitle.insertAdjacentHTML('afterend', sPrice.outerHTML);
})
var st = doc.createElement('link');
st.href = "https://fonts.googleapis.com/css?family=Roboto+Slab:300,500,800&display=swap&subset=cyrillic";
st.rel = "stylesheet";
doc.head.appendChild(st);
