var doc = this.document;
doc.addEventListener('DOMContentLoaded', function(){
    var standartPack = doc.querySelectorAll('.form-position')[0];
    var standartInput = standartPack.getElementsByTagName('input')[0];
    var extendedPack = doc.querySelectorAll('.form-position')[1];
    var extendedInput = extendedPack.getElementsByTagName('input')[0];
    var priceBlock = doc.querySelector('.custom-show-price');

    function changePrice (event) {
        if (event.target.value) {
            var price = event.target.parentNode.parentNode.querySelector('.form-position-price').innerHTML;
            var vacant = event.target.parentNode.parentNode.querySelector('.stream-vacant').outerHTML;
            priceBlock.innerHTML = price;
            priceBlock.insertAdjacentHTML('afterbegin', vacant);
        }
    }

    standartInput.addEventListener('change', changePrice);
    extendedInput.addEventListener('change', changePrice);
    standartInput.click();
    var sPrice = standartPack.querySelector('.final-price');
    var ePrice = extendedPack.querySelector('.final-price');
    var sTitle = standartPack.querySelector('.offer-title');
    var eTitle = extendedPack.querySelector('.offer-title');

    sTitle.insertAdjacentHTML('afterend', sPrice.outerHTML);
    eTitle.insertAdjacentHTML('afterend', ePrice.outerHTML);
})
var st = doc.createElement('link');
st.href = "https://fonts.googleapis.com/css?family=Roboto+Slab:300,500,800&display=swap&subset=cyrillic";
st.rel = "stylesheet";
doc.head.appendChild(st);
