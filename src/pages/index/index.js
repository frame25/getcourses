import './index.sass'

var isFramed = window != window.top || document != top.document || self.location != top.location;

if (!isFramed) {
    // HEADER
    var newHeader = document.createElement('div');
    var oldHeader = document.querySelector('.gc-account-logo');
    var container = document.querySelector('.main-page-block');
    var tpl = `<div id="my-custom-menu">
<div id="section-menu">
    <div id="main-logo"></div>
    
    <a href="//alter-arts.ru/online_course" class="menu__item">
    <span class="item__text">Курсы</span>
    </a>
    
    <div class="menu__item dropdown">
    <span class="item__text">Мастер-классы</span>
     <div class="menu__submenu">
        <a href="//alter-arts.ru/workshop" class="submenu__item">Геод</a>
        <!--<a href="//alter-arts.ru/mk/picture" class="submenu__item">Картина</a>-->
        <!--<a href="//alter-arts.ru/mk/table" class="submenu__item">Стол</a>-->
    </div>
    </div>

    <a href="//app.alter-arts.ru/teach/control" class="menu__item auth-only">
    <span class="item__text">Мое обучение</span>
    </a>
    
</div><!--end:section-menu-->

<div id="section-user" class="auth-only">
    <div id="user__avatar"><!--из жски--></div>
    <div id="user__name"><!--из жски-->Name</div>
    <div class="user__menu">
        <a href="//app.alter-arts.ru/sales/control/userProduct/my" class="menu__item">Мои покупки</a>
        <a href="//app.alter-arts.ru/user/my/profile" class="menu__item">Настройки</a>
        <a href="#" id="custom-logout" class="menu__item">Выход</a>
    </div>
</div>

<div id="section-login">
    <a href="//app.alter-arts.ru/login" class="login__link">Вход / Регистрация</a>
</div>
</div>`;

    newHeader.innerHTML = tpl;
    try {
        container.insertBefore(newHeader, container.firstChild);
        oldHeader.style.display = 'none';
    } catch(e){console.log('can not replace header')}

// USER PROPS
    var userProps = window.localStorage.getItem('user_props');
    var loginSection = document.getElementById('section-login');
    if (userProps) {
        userProps = JSON.parse(userProps);
        var avatar = document.getElementById('user__avatar');
        var uName = document.getElementById('user__name');
        var logout = document.getElementById('custom-logout');
        avatar.style.backgroundImage = 'url(' + userProps.avatar + ')';
        uName.innerText = userProps.userName;
        loginSection.style.display = 'none';
        logout.addEventListener('click', event => {
            event = event || window.event;
            event.preventDefault();
            localStorage.removeItem('user_props');
            window.open('//app.alter-arts.ru/user/my/logout', '_self');
        })
    } else {
        document.querySelectorAll('.auth-only').forEach(function(el){ el.style.display = 'none' })
    }

    // удаление стандартного меню
    window.addEventListener('load', function(){
        var leftbar = document.querySelector('.gc-account-leftbar');
        leftbar.style.display = 'none';
    })

}
